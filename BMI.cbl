       IDENTIFICATION DIVISION.
       PROGRAM-ID. bmi.
       AUTHOR. KASAMA.

       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  WEIGHT  PIC   9(3)V99.
       01  HEIGHT  PIC   9(3)V99.
       01  BMI     PIC   9(2)V99.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter your weight = " WITH NO ADVANCING
           ACCEPT WEIGHT
           DISPLAY "Enter your height = " WITH NO ADVANCING
           ACCEPT HEIGHT
           COMPUTE HEIGHT = HEIGHT / 100
           COMPUTE BMI = (WEIGHT / (HEIGHT * HEIGHT))
           DISPLAY BMI
           IF BMI < 18.50 THEN
              DISPLAY "You are Underweight"
           ELSE
              IF BMI >= 18.50 AND BMI < 22.90 THEN
                 DISPLAY "You are Normal(Healthy)"
              END-IF
              IF BMI >= 22.90 AND BMI < 24.90 THEN
                 DISPLAY "You are Overweight"
              END-IF
              IF BMI >= 24.90 AND BMI <= 29.90 THEN
                 DISPLAY "You have Obese Class 1"
              END-IF 
              IF BMI > 30 THEN
                 DISPLAY "You have Obese Class 2 or more"
              END-IF
           END-IF
           GOBACK 
           .
