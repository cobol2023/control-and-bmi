       IDENTIFICATION DIVISION. 
       PROGRAM-ID.  control4.
       AUTHOR. KASAMA.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT STUDENT-FILE ASSIGN TO "GRADE.DAT"
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION.
       FILE SECTION.
       FD  STUDENT-FILE.
       01  STUDENT-DETAILS.
           88 END-OF-STUDENT-FILE  VALUE HIGH-VALUE.
           05 STUDENT-ID     PIC X(8).
           05 STUDENT-NAME   PIC X(16).
           05 COURSE-CODE    PIC X(8).
           05 GRADE          PIC X(2).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT  STUDENT-FILE
      *    READ STUDENT-FILE
      *       AT END SET  END-OF-STUDENT-FILE TO TRUE
      *    END-READ

           PERFORM  UNTIL END-OF-STUDENT-FILE
              READ STUDENT-FILE
                 AT END SET  END-OF-STUDENT-FILE TO TRUE
              END-READ
              IF END-OF-STUDENT-FILE THEN
                 DISPLAY  "END"
              END-IF
              DISPLAY "D"
                 STUDENT-NAME SPACE STUDENT-ID SPACE COURSE-CODE
                 SPACE GRADE
           END-PERFORM
           CLOSE STUDENT-FILE 
           .
